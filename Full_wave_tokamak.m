clear

%This script only contains a limited amount of plots

%--- INIT ---

% Nature constants
natconst;

%Wave frequency
FREQ = 2.45e9;

% vacuum wave vector
lambda0 = C/FREQ;
k0 = 2*pi/lambda0;


%------USER INPUT-------

% Number of time steps
nt = 3000;

% Toroidal launch angle
theta = (120)*pi/180;

% Wave mode
MODE = 'O';


% Grid dimensions
y_dim = 1.1;
x_dim = 0.8;

% central position of Gaussin
x0 = -0.22;

% The position of the waveguide is defined by the toroidal launch angle.
% The central position if the Gaussian os important for correctly launching
% the Gaussian beam into the waveguide. It has to be tweeked for optimal
% results.

% Beam width. This is not the width of teh waveguide, but the width of the
% beam exited in the end of teh waveguide
width = lambda0/1.5;

% Peak electric field size in beam
EE0 = 2;


% ----INIT----

%Set up video
v = VideoWriter('waveguide_no_plasma.avi');
v.FrameRate = 8;
open(v);




% Angular source frequancy
w = 2*pi*FREQ;
w2 = (2*pi*FREQ)^2;

% Grid spacing
S= 1/(2);
dt_ds = 1/1*S/C;
ds = lambda0/(40);
dt = dt_ds*ds;

% Grid size
nx = ceil(x_dim/ds);
ny = ceil(y_dim/ds);

% Construction of grid containing plasma frequancy
[XXX, YYY] = meshgrid(-0.7:ds:0.4, -0.4:ds:0.4);
R = sqrt(XXX.^2+YYY.^2);
wpe2 = 0*1.5*w2.*(1-(R-0.25).^2/0.125^2);
wpi2 = wpe2*Me/(2*Mp);

for i=1:1:nx
    for j=1:1:ny
        if wpe2(i,j)<0
            wpe2(i,j) = 0;
            wpi2(i,j) = 0;
        end
    end
end

wp2 = wpe2+wpi2;

% grid containing X-coordinate
X = wpe2/w2;

% Construction of stationary B-field
B0x = -0.02./R.*sin(atan2(YYY,XXX));
B0y = 0.02./R.*cos(atan2(YYY,XXX));
B0x_unit = B0x./sqrt(B0x.^2+B0y.^2);
B0y_unit = B0y./sqrt(B0x.^2+B0y.^2);
B0 = sqrt(B0y.^2+B0x.^2);

% Grid containing Y-coordinate
Y = Qe*B0(floor((x0+0.5)/ds),100)/(Me*w);

% Grid containing electron cyclotron frequency
wce = Qe.*sqrt(B0x.^2+B0y.^2)/Me;
wci = Qe.*sqrt(B0x.^2+B0y.^2)/(2*Mp);

% Grid containing Upper hybrid frequencies
w_UH = sqrt(wce.^2+wpe2);
w_R = (wce-wci)/2+(((wce+wci)/2).^2+wp2).^(1/2);
wR2 = w_R.^2;


% Vector containing horizontal positions
x = linspace(-x_dim/2,x_dim/2,nx);

% Product of w and dt
wt = 2*pi*C/lambda0*dt;

% Solving matrix wave equation in vacuum
SS = 1;
D = 0;
P = 1;
N = 1;
K = [[SS-N^2*cos(theta)^2, -1i*D, N^2*cos(theta)*sin(theta)]; [1i*D, SS-N^2, 0]; [N^2*cos(theta)*sin(theta), 0, P-N^2*sin(theta)^2]];

a = null(K);

% The two solutions are the polarization of O-mode and X-mode
EO = a(:,1);
EX = a(:,2);

% Choosing either O or X-mode based on user input
if MODE == 'O'
    Exx = EO(3);
    Eyy = EO(1);
    Ezz = EO(2);
elseif MODE == 'X'
    Exx = EX(3);
    Eyy = EX(1);
    Ezz = EX(2);
end


% --- Constructing of a matrix with ones everywhere exept zeros at teh position of 
% the vacuum vessel and wave guide---- 

% The vessel has dimentions like NORTH, and the waveguide a width of
% 0.086m

% The matrix (called VesselMatrix) is used to provide absorbing boundary
% conditions

VesselMatrix = ones(nx,ny);
opening = 0.06365;
lower = -0.37;
i_list = [];
i_list2 = [];
 for i=1:nx
    for j=1:ny
        u = abs(((i-nx/2)*ds)^2+((j-(nx/2+ny-nx))*ds)^2-(0.125)^2);
        if u<0.0010
            VesselMatrix(i,j) = 0;
        end
        u = abs(((i-nx/2)*ds)^2+((j-(nx/2+ny-nx))*ds)^2-(0.377)^2);
        if abs((i-nx/2)*ds) > opening
            if u<0.003
                VesselMatrix(i,j) = 0;
            end
        elseif j*ds>x_dim/2
            if u<0.002
                VesselMatrix(i,j) = 0;
            end
        end
        if j*ds < 0.7+lower
            u = abs(((i-nx/2)*ds-opening)-tan(pi/2-(pi-theta))*((j*ds)-(0.7+lower)));
            if u<0.006
                VesselMatrix(i,j) = 0;
                if j == 1
                    i_list = [i_list, i];
                end
            end
        end
        if j*ds < 0.7+lower
            u = abs(((i-nx/2)*ds+opening)-tan(pi/2-(pi-theta))*((j*ds)-(0.7+lower)));
            if u<0.006
                VesselMatrix(i,j) = 0;
                if j == 1
                    i_list2 = [i_list2, i];
                end
            end
        end
    end
end


% Emty matries for E, B and J
Ex=zeros(nx,ny);
Ey=zeros(nx,ny);
Ez=zeros(nx,ny);

Bx=zeros(nx,ny);
By=zeros(nx,ny);
Bz=zeros(nx,ny);

Jx=zeros(nx,ny);
Jy=zeros(nx,ny);
Jz=zeros(nx,ny);



%loop through time
for n=1:1:nt
    if mod(n,50) == 0
        disp(n)
    end
    
    
    %Iteration
    
    % Update equations
    Bx(2:end-3,2:end-3) = Bx(2:end-3,2:end-3) - dt_ds*(Ez(2:end-3,3:end-2) - Ez(2:end-3,2:end-3));
    By(2:end-3,2:end-3) = By(2:end-3,2:end-3) + dt_ds*(Ez(3:end-2,2:end-3) - Ez(2:end-3,2:end-3));
    Ez(3:end-3,3:end-3) = Ez(3:end-3,3:end-3) + dt_ds*C^2*(By(3:end-3,3:end-3)-By(2:end-4,3:end-3) - (Bx(3:end-3,3:end-3) - Bx(3:end-3,2:end-4))) - dt/E0*Jz(3:end-3,3:end-3);


    Bz(3:end-3,3:end-3) = Bz(3:end-3,3:end-3) + dt_ds*(-(Ey(3:end-3,3:end-3) - Ey(2:end-4,3:end-3)) +  (Ex(3:end-3,3:end-3) - Ex(3:end-3,2:end-4)));
    Ex(2:end-3,2:end-3) = Ex(2:end-3,2:end-3) + dt_ds*C^2*(Bz(2:end-3,3:end-2) - Bz(2:end-3,2:end-3)) - dt/E0*Jx(2:end-3,2:end-3);
    Ey(2:end-3,2:end-3) = Ey(2:end-3,2:end-3) - dt_ds*C^2*(Bz(3:end-2,2:end-3) - Bz(2:end-3,2:end-3)) - dt/E0*Jy(2:end-3,2:end-3);
    
  
    Jx(2:end-3,2:end-3) = Jx(2:end-3,2:end-3) + dt*(E0*wpe2(2:end-3,2:end-3).*Ex(2:end-3,2:end-3) - wce(2:end-3,2:end-3).*(-Jz(2:end-3,3:end-2).*B0y_unit(2:end-3,3:end-2)));
    Jy(2:end-3,2:end-3) = Jy(2:end-3,2:end-3) + dt*(E0*wpe2(2:end-3,2:end-3).*Ey(2:end-3,2:end-3) - wce(2:end-3,2:end-3).*(Jz(3:end-2,2:end-3).*B0x_unit(3:end-2,2:end-3)));
    Jz(3:end-2,3:end-2) = Jz(3:end-2,3:end-2) + dt*(E0*wpe2(3:end-2,3:end-2).*Ez(3:end-2,3:end-2) - wce(3:end-2,3:end-2).*(Jx(3:end-2,2:end-3).*B0x_unit(3:end-2,3:end-2)-Jy(2:end-3,3:end-2).*B0x_unit(3:end-2,3:end-2)));
    
    
     
    %Exitation of source
    start = i_list2(2)+1; 
    endd = i_list(2)-1;
    
    Ex(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Exx.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
    Ey(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Eyy.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
    Ez(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Ezz.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
 
    %Insuring correct angle of initial beam
    x = x+0.45;
    t = [find(x(x(start:1:endd)>0+abs(C/(-cos(theta))*n*dt)))+(nx)-length(find(x(x>C/(-cos(theta))*n*dt)))];
    x = x-0.45;
    
    if n<200
        if length(t)>10
            for m=1:10
                if t(1) ~= 1
                    t = [t(1)-1,t];
                end
            end
        end
    end
    
     Ex(t+start, 3) = 0;
     Ey(t+start, 3) = 0;
     Ez(t+start, 3) = 0;



% Absorbing boundary conditions at the Vessel walls and waveguide

Ey = VesselMatrix.*Ey;
Ex = VesselMatrix.*Ex;
Ez = VesselMatrix.*Ez;


% Plot used in video. The time spacing between frames and when to start the video is defined here
if n>1
    if mod(n,25) == 0
        figure(1)
        clf;
        hold on
        E = 0.3*(abs(Ey)+abs(Ex)+abs(Ez));
        imagesc(ds*(1:1:nx)-x_dim/2,(ds*(1:1:ny)-x_dim/2-0.3)',E',[0,0.5]);colorbar;
        R0 = 0.25; a0=0.125; ang=0:0.01:2*pi; ang1 = (-pi/2+0.18):0.01:(3*pi/2-0.18);
        axis equal
        colormap jet
        xlabel('x [m]','FontSize',20);
        ylabel('y [m]','FontSize',20);
        set(gca,'FontSize',16);
        plot((R0-a0)*cos(ang),(R0-a0)*sin(ang),'k','linewidth',2);
        plot((R0+a0)*cos(ang1),(R0+a0)*sin(ang1),'k','linewidth',2);
        [c,h] = contour(XXX,YYY,w_R,[w w/5]);
        rc = [0.0 0.0 0.7];
        set(h(1),'EdgeColor',rc,'linewidth',1.5);
        [c,h1] = contour(XXX,YYY,w_UH,[w w/5]);
        rc = [0.7 0 0];
        set(h1(1),'EdgeColor',rc,'linewidth',1.5);
        [c,h2] = contour(XXX,YYY,sqrt(wp2),[w, w/10000]);
        rc = 'k';
        set(h2(1),'EdgeColor',rc,'linewidth',2);
        plot([i_list(2)*ds-x_dim/2, opening], [-0.7, lower], 'LineWidth', 2, 'Color', 'k')
        plot([i_list2(2)*ds-x_dim/2, -opening], [-0.7, lower],  'LineWidth', 2, 'Color', 'k')
        title(['\fontsize{20} Time = ',num2str(round(n*dt*1e+12)),' ps', ', n=',num2str(n), ', E']); 
        xlim([-0.4, 0.4])
        ylim([-0.7, 0.4])
        frame = getframe(gcf);
        writeVideo(v,frame);
    end
end
end

%Closing the video object
close(v)
 

% ------ Plottong ----
%% Plotting the absolute value of teh electric field
figure(2)
clf;
hold on
imagesc(ds*(1:1:nx)-x_dim/2,(ds*(1:1:ny)-x_dim/2-0.3)',abs(Ex)',[0,1.0]*1.0);colorbar;
axis equal
colormap jet
title(['\fontsize{20} Time = ',num2str(round(n*dt*1e+12)),' ps', ', n=',num2str(n), ', Ex']); 
xlabel('x [m]','FontSize',20);
ylabel('y [m]','FontSize',20);
set(gca,'FontSize',16);
R0 = 0.25;a0=0.125;
ang=0:0.01:2*pi;
plot((R0-a0)*cos(ang),(R0-a0)*sin(ang),'k','linewidth',4);
ang1 = (-pi/2+0.165):0.01:(3*pi/2-0.165);
plot((R0+a0)*cos(ang1),(R0+a0)*sin(ang1),'k','linewidth',4);
[c,h] = contour(XXX,YYY,w_R,[w w/5]);
rc ='w';
set(h(1),'EdgeColor',rc,'linewidth',2, 'LineStyle','--');
[c,h1] = contour(XXX,YYY,w_UH,[w w/5]);
rc = 'w';
set(h1(1),'EdgeColor',rc,'linewidth',2, 'LineStyle','--');
ylim([-0.5,-0.2])
xlim([-0.2, 0.25])
plot((0.32219)*cos(ang),(0.32219)*sin(ang),'w','linewidth',2,'LineStyle','--');
plot([i_list(2)*ds-x_dim/2, opening], [-0.7, lower], 'LineWidth', 4, 'Color', 'k')
plot([i_list2(2)*ds-x_dim/2, -opening], [-0.7, lower],  'LineWidth', 4, 'Color', 'k')

%% Plotting the absolute value of teh electric field
figure(3)
clf;
hold on
E = sqrt(abs(Ey).^2+abs(Ex).^2+abs(Ez).^2);
EE = E;
EE = EE.*VesselMatrix;
imagesc(ds*(1:1:nx)-x_dim/2,(ds*(1:1:ny)-x_dim/2-0.3)',EE',[0,0.5]);colorbar;
axis equal
colormap jet
title(['\fontsize{20} Time = ',num2str(round(n*dt*1e+12)),' ps', ', n=',num2str(n), ', E']); 
xlabel('x [m]','FontSize',20);
ylabel('y [m]','FontSize',20);
set(gca,'FontSize',16);
plot((R0-a0)*cos(ang),(R0-a0)*sin(ang),'k','linewidth',4);
plot((R0+a0)*cos(ang1),(R0+a0)*sin(ang1),'k','linewidth',4);
% [c,h] = contour(XXX,YYY,w_R,[w w/5]);
% rc = [0.0 0.0 0.7];
% rc = 'w';
% set(h(1),'EdgeColor',rc,'linewidth',2);
% [c,h1] = contour(XXX,YYY,w_UH,[w w/5]);
% rc = [0.7 0 0];
% rc = 'w';
% set(h1(1),'EdgeColor',rc,'linewidth',2);
% [c,h2] = contour(XXX,YYY,sqrt(wp2),[w, w/10000]);
% rc = 'k';
% set(h2(1),'EdgeColor',rc,'linewidth',2);
plot([i_list(2)*ds-x_dim/2, opening], [-0.7, lower], 'LineWidth', 4, 'Color', 'k')
plot([i_list2(2)*ds-x_dim/2, -opening], [-0.7, lower],  'LineWidth', 4, 'Color', 'k')
% plot((0.32219)*cos(ang),(0.32219)*sin(ang),'w','linewidth',2);
ylim([-0.4,0.4])
xlim([-0.4, 0.4])