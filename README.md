All codes are now commented.

The script Ray_tracing_main is the main scribt for the Ray tracing analysis.
It uses teh function Ray_transmission for calculating the transmission coefficient 
and Ray_reflection for calculating the fraction of power reflected as O-mode and X-mode respectively.
Finally it uses teh function Ray_CMA_plot to construct a CMA diagram.

The script Full_wave_slab simulates the propagation of a Gaussian beam in a slab geometry ang can be used to stydy O-X conversion.

The script Full_wave_tokamak simulated the propagation of a wave through a waveguide into a tokamak with a geometry like NORTH.