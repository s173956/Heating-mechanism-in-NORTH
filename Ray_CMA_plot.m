function CMA_plot(equifile,DS,BS,frq,machine,N,ret,i, mode,figno, map, power)

%Check input variables
if ~exist('DS') DS = 1; end
if ~exist('BS') BS = 1; end
if ~exist('frq') frq = 105; end
if ~exist('machine') machine = 'ASDEX'; end
if ~exist('figno') figno = 60; end

if ~isstruct(equifile)
    a = load(equifile);
else
    a = equifile;
    disp('equi struct supplied by user')
end

%------ INIT ------

% Source frequency
w = frq*2*pi;

%Nature constants
natconst;

%Mass of ion species
massratio=2*1836;

% Spatial grids with plasma electron frequency and size of stationary B-field
ne_grid = interp1(a.psi_prof,a.ne_prof, a.psi_grid)*1e19;
Bmod_grid = BS * sqrt(a.BR_grid.^2 + a.Bz_grid.^2 + a.Bphi_grid.^2);

% Grids for plasma frequency and electron cyclotron frequency
wpe_grid =  sqrt(DS*ne_grid*Qe.^2/(E0*Me));
wpi_grid = wpe_grid/sqrt(massratio);
wp_grid = sqrt(wpe_grid.^2+wpi_grid.^2);
wce_grid = Qe*Bmod_grid/Me;

%Spatial coordinates
radial = transpose(sqrt(ret(N).r(1:(end-i),1).^2+ret(N).r(1:(end-i),2).^2));
z = transpose(ret(N).r(1:(end-i),3));

%Spatial meshgrid
[R,Z] = meshgrid(a.R_grid,a.z_grid);

%Lists with plasma frequency and electron cyclotron frequency
wp_list = interp2(R,Z, wp_grid , radial, z);
wce_list = interp2(R,Z, wce_grid, radial, z);

% Convert lists to X,Y coordinates
X = wp_list.^2/w.^2;
Y = wce_list/w;


%----- MAKING DIAGRAM ------
hold on;
set(gca,'fontsize',16);

% Remove empty entities
X = X(~isnan(X))';
Y = Y(~isnan(Y))';

% Plot the list of X and Y components (different styles for O- and X-mode)
if mode == 'O'
    plot(X,Y,'-','LineWidth', 1.5,'color', map(ceil(100*power),:));
elseif mode == 'X'
    plot(X,Y,':','LineWidth', 1.5,'color', map(ceil(100*power),:));
end

% Plot all boundaries in the CMA diagram. These are calculated outside of
% MATLAB
a = 1/massratio;
x = linspace(0,1,100);
X = x*1/(a^2+1);

y1 = -(3671*X)/7344 - 3671/2 + sqrt(13476241*X.^2 - 99077382576*X + 181906074409536)/7344;
plot(x,y1,'LineWidth', 1.5, 'color', 'k');

x3 = ones(100);
y2 = linspace(0,2.5,100);
plot(x3,y2, 'LineWidth', 1.5, 'color', 'k')

x1 = linspace(1,2.0,50);
X1 = x1*1/(a^2+1);
y3 = (3671*X1)/7344 + 3671/2 - sqrt(13476241*X1.^2 - 99077382576*X1 + 181906074409536)/7344;
plot(x1,y3, 'LineWidth', 1.5, 'color', 'k')

y4 = ones(100);
x2 = linspace(0,2.0,100);
plot(x2,y4,'LineWidth', 1.5, 'color', 'k');

y5 = real(sqrt(26967170 - 2*sqrt(4*X.^2 + 181807010517889) - 4*X)/2);
plot(x,y5,'LineWidth', 1.5, 'color', 'k');

xlabel('$X$','Interpreter','latex', 'FontSize', 34)
ylabel('$Y$','Interpreter','latex','FontSize', 34)
hYLabel = get(gca,'YLabel');
set(hYLabel,'rotation',0,'VerticalAlignment','middle')


% Insert text describing the cutoffs and resonances in the plot
text(1.02,0.6,'$P=0$','Interpreter','latex', 'FontSize', 16)
text(1.30,1.08,'$R\rightarrow \infty$','Interpreter','latex', 'FontSize', 16)
text(0.20,0.42,'$R = 0$','Interpreter','latex', 'FontSize', 16)
text(0.60,0.70,'$S = 0$','Interpreter','latex', 'FontSize', 16)
text(1.29,0.2,'$L = 0$','Interpreter','latex', 'FontSize', 16)
set(gca,'FontSize',18);

end