function [T, L, N2z, N2y, N2z_opt, YY] = Ray_transmission(equifile,DS,BS,frq,N,ret,i)

    %Check input variables
    if ~isstruct(equifile)
        a = load(equifile);
    else
        a = equifile;
        disp('equi struct supplied by user')
    end

    %------Define grids and variables
    
    % Source frequency
    w = frq*2*pi;
    
    %Vacuum wavelength
    k0 = 51.31268000;
    
    %Nature constants
    natconst;
    
    %Mass of ion species
    massratio=2*1836;

    %Grid with plasma electron density
    ne_grid = interp1(a.psi_prof,a.ne_prof, a.psi_grid)*1.248*1e19*DS;
    
    %Calculating the size of the gradient
    [gz, gR] = gradient(ne_grid,0.025,0.011);
    gmod = sqrt((gz).^2+(gR).^2);

    %Size of magnetic field
    Bmod_grid = BS * sqrt(a.BR_grid.^2 + a.Bz_grid.^2 + a.Bphi_grid.^2);

    % Grids for plasma frequency and electron cyclotron frequency
    wpe_grid =  sqrt(DS*ne_grid*Qe.^2/(E0*Me));
    wpi_grid = wpe_grid/sqrt(massratio);
    wp_grid = sqrt(wpe_grid.^2+wpi_grid.^2);
    wce_grid = Qe*Bmod_grid/Me;

    %Spatial coordinates
    radial = transpose(sqrt(ret(N).r(1:(end-i),1).^2+ret(N).r(1:(end-i),2).^2));
    z = transpose(ret(N).r(1:(end-i),3));

    %Spatial meshgrid
    [R,Z] = meshgrid(a.R_grid,a.z_grid);

    %Lists with plasma frequency and electron cyclotron frequency
    wp_list = interp2(R,Z, wp_grid , radial, z);
    wce_list = interp2(R,Z, wce_grid, radial, z);

    % Convert lists to X,Y coordinates
    X = wp_list.^2/w.^2;
    Y = wce_list/w;

    %Current position
    Rc = sqrt(ret(N).r(end-i,1).^2+ret(N).r(end-i,2).^2);
    zc = ret(N).r(end-i,3);

    % Interpolate length scale for the density gradient
    L = interp2(R,Z, ne_grid , Rc, zc)/interp2(R,Z, gmod , Rc, zc);
    
    %Calculating the optimal squared Nz
    N2z_opt = Y(end)/(1+Y(end));
    
    %Current refractive index
    Nvec = C/w*[ret(N).k(end-i,1), ret(N).k(end-i,2), ret(N).k(end-i,3)];
    
    %Y-component of refractive index (in STIX geometry)
    N2y = Nvec(3).^2;
    
    %Incident angle
    phi = atan2(ret(N).r(end-i,2), ret(N).r(end-i,1));
    
    % Z-component of refractive index (in STIX geometey)
    N2z = (-sin(phi)*Nvec(1)+cos(phi)*Nvec(2)).^2;

    % Current Y coordinate
    YY = Y(end);
    
    %Calculating O-X transmission coefficient
    T = exp(-pi*k0*L*sqrt(Y(end)/2)*( (1+Y(end)).^2/(2*Y(end))*(N2z_opt-N2z).^2+N2y));
    if isnan(T)
        T=0.001;
    end
end