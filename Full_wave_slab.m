clear

%This script only contains a limited amount of plots

% ----INIT----

% Nature constants
natconst;

%Wave frequency
FREQ = 2.45e9;

% vacuum wave vector and wavelength
lambda0 = C/FREQ;
k0 = 2*pi/lambda0;



%------USER INPUT-------

% Number of time steps
nt = 800;

% Toroidal launch angle
theta = (132.5)*pi/180;

% Wave mode
MODE = 'O';

% Grid dimensions
y_dim = 0.8;
x_dim = 5;

% Launch position
x0 = 2.1;
% Exact start and end position for exiting the beam (start, endd) has to be tweeked for
% optimal results and are defined in lines 237-238

% Beam width
width = 4*lambda0;

% Peak electric field size in beam
EE0 = 2;


%---- INIT CONTINUED

% Angular source frequancy
w = 2*pi*FREQ;
w2 = (2*pi*FREQ)^2;

% Grid spacing
S= 1/(2);
dt_ds = 1/1*S/C;
ds = lambda0/(40);
dt = dt_ds*ds;

% Grid size
nx = ceil(x_dim/ds);
ny = ceil(y_dim/ds);

% Stationary magnetic field matrix
B0 = 0.07;
B0x=B0*ones(nx,ny);
B0y=zeros(nx,ny);
B0z=zeros(nx,ny);

% Direction of stationary field
B0x_unit = B0x./B0;

% Grids containing plasma frequancy
wpe2 = zeros(nx,ny);
wpi2 = zeros(nx,ny);
wpe2(:,20:end) = 1.0*1e17.*ones(nx,ny-19).*(0.5*(1-cos(linspace(0,1,ny-19)*pi)))*Qe^2/(Me*E0);
wpi2(:,20:end) = 1.0*1e17.*ones(nx,ny-19).*(0.5*(1-cos(linspace(0,1,ny-19)*pi)))*Qe^2/(2*Mp*E0);
wp2 = wpe2+wpi2;

% Grid containing electron cyclotron frequency
wce = Qe*B0x/Me;
wci = Qe*B0x/(2.*Mp);

% Grid containing Upper hybrid frequencies
w_UH = sqrt(wce.^2+wpe2);
w_R = (wce-wci)/2+(((wce+wci)/2).^2+wp2).^(1/2);
wR2 = w_R.^2;

% grid containing X-coordinate
X = wpe2/w2;

% Y-coordinate
Y = Qe*B0/(Me*w);

% Vector containing horizontal positions
x = linspace(0,x_dim,nx);

% Product of w and dt
wt = 2*pi*C/lambda0*dt;

% ---Polarization of O- and X-mode---

% Solving matrix wave equation in vacuum
SS = 1;
D = 0;
P = 1;
N = 1;
K = [[SS-N^2*cos(theta)^2, -1i*D, N^2*cos(theta)*sin(theta)]; [1i*D, SS-N^2, 0]; [N^2*cos(theta)*sin(theta), 0, P-N^2*sin(theta)^2]];

a = null(K);

% The two solutions are the polarization of O-mode and X-mode
EO = a(:,1);
EX = a(:,2);

% Choosing either O or X-mode based on user input
if MODE == 'O'
    Exx = EO(3);
    Eyy = EO(1);
    Ezz = EO(2);
elseif MODE == 'X'
    Exx = EX(3);
    Eyy = EX(1);
    Ezz = EX(2);
end

% Factors to use for calculating boundary conditions
K1 = (1-1/S)/(1+1/S);
K2 = (2-S^2)/(S+1);
K3 = S/(2+2/S);


%-- Main grid --

% Emty matries for E, B and J
Ex=zeros(nx,ny);
Ey=zeros(nx,ny);
Ez=zeros(nx,ny);

Bx=zeros(nx,ny);
By=zeros(nx,ny);
Bz=zeros(nx,ny);

Jx=zeros(nx,ny);
Jy=zeros(nx,ny);
Jz=zeros(nx,ny);


% Vectors for storing boundary conditions
Ex_lower_p_1 = zeros(nx,1);
Ex_lower_p_0 = zeros(nx,1);
Ex_upper_p_1 = zeros(nx,1);
Ex_upper_p_0 = zeros(nx,1);
Ex_right_p_1 = zeros(1,ny);
Ex_right_p_0 = zeros(1,ny);
Ex_left_p_1 = zeros(1,ny);
Ex_left_p_0 = zeros(1,ny);

Ey_lower_p_1 = zeros(nx,1);
Ey_lower_p_0 = zeros(nx,1);
Ey_upper_p_1 = zeros(nx,1);
Ey_upper_p_0 = zeros(nx,1);
Ey_right_p_1 = zeros(1,ny);
Ey_right_p_0 = zeros(1,ny);
Ey_left_p_1 = zeros(1,ny);
Ey_left_p_0 = zeros(1,ny);

Ez_lower_p_1 = zeros(nx,1);
Ez_lower_p_0 = zeros(nx,1);
Ez_upper_p_1 = zeros(nx,1);
Ez_upper_p_0 = zeros(nx,1);
Ez_right_p_1 = zeros(1,ny);
Ez_right_p_0 = zeros(1,ny);
Ez_left_p_1 = zeros(1,ny);
Ez_left_p_0 = zeros(1,ny);



% -- REFERENCE GRID --

% y-dimension of reference grid
ny_r = ceil(0.3/ds);

% Emty matries for E and B in the referene grid
Ex_r=zeros(nx,ny_r);
Ey_r=zeros(nx,ny_r);
Ez_r=zeros(nx,ny_r);

Bx_r=zeros(nx,ny_r);
By_r=zeros(nx,ny_r);
Bz_r=zeros(nx,ny_r);


% Empty matrices for boundary conditions
Ex_r_upper_p_1 = zeros(nx,1);
Ex_r_upper_p_0 = zeros(nx,1);
Ey_r_upper_p_1 = zeros(nx,1);
Ey_r_upper_p_0 = zeros(nx,1);
Ez_r_upper_p_1 = zeros(nx,1);
Ez_r_upper_p_0 = zeros(nx,1);


% Empty lists to keep track of ingoing power, outgoing power and the conversion rate
power_in_tot = zeros(1526,1);
power_out_tot = zeros(1526,1);
Efficiency_list = [];


%loop through time
for n=1:1:nt
    if mod(n,50) == 0
        disp(n)
    end
    

    % Update equations
    Bx(2:end-3,2:end-3) = Bx(2:end-3,2:end-3) - dt_ds*(Ez(2:end-3,3:end-2) - Ez(2:end-3,2:end-3));
    By(2:end-3,2:end-3) = By(2:end-3,2:end-3) + dt_ds*(Ez(3:end-2,2:end-3) - Ez(2:end-3,2:end-3));
    Ez(3:end-3,3:end-3) = Ez(3:end-3,3:end-3) + dt_ds*C^2*(By(3:end-3,3:end-3)-By(2:end-4,3:end-3) - (Bx(3:end-3,3:end-3) - Bx(3:end-3,2:end-4))) - dt/E0*Jz(3:end-3,3:end-3);
  
    Bz(3:end-3,3:end-3) = Bz(3:end-3,3:end-3) + dt_ds*(-(Ey(3:end-3,3:end-3) - Ey(2:end-4,3:end-3)) +  (Ex(3:end-3,3:end-3) - Ex(3:end-3,2:end-4)));
    Ex(2:end-3,2:end-3) = Ex(2:end-3,2:end-3) + dt_ds*C^2*(Bz(2:end-3,3:end-2) - Bz(2:end-3,2:end-3)) - dt/E0*Jx(2:end-3,2:end-3);
    Ey(2:end-3,2:end-3) = Ey(2:end-3,2:end-3) - dt_ds*C^2*(Bz(3:end-2,2:end-3) - Bz(2:end-3,2:end-3)) - dt/E0*Jy(2:end-3,2:end-3);

    Jx(2:end-3,2:end-3) = Jx(2:end-3,2:end-3) + dt*(E0*wpe2(2:end-3,2:end-3).*Ex(2:end-3,2:end-3));
    Jy(2:end-3,2:end-3) = Jy(2:end-3,2:end-3) + dt*(E0*wpe2(2:end-3,2:end-3).*Ey(2:end-3,2:end-3) - wce(2:end-3,2:end-3).*(Jz(3:end-2,2:end-3).*B0x_unit(2:end-3,2:end-3)));
    Jz(3:end-2,2:end-3) = Jz(3:end-2,2:end-3) + dt*(E0*wpe2(3:end-2,2:end-3).*Ez(3:end-2,2:end-3) - wce(3:end-2,2:end-3).*(-Jy(2:end-3,2:end-3).*B0x_unit(3:end-2,2:end-3)));


    %Reference grid update equations
    Bx_r(2:end-3,2:end-3) = Bx_r(2:end-3,2:end-3) - dt_ds*(Ez_r(2:end-3,3:end-2) - Ez_r(2:end-3,2:end-3));
    By_r(2:end-3,2:end-3) = By_r(2:end-3,2:end-3) + dt_ds*(Ez_r(3:end-2,2:end-3) - Ez_r(2:end-3,2:end-3));
    Ez_r(3:end-3,3:end-3) = Ez_r(3:end-3,3:end-3) + dt_ds*C^2*(By_r(3:end-3,3:end-3)-By_r(2:end-4,3:end-3) - (Bx_r(3:end-3,3:end-3) - Bx_r(3:end-3,2:end-4)));

    Bz_r(3:end-3,3:end-3) = Bz_r(3:end-3,3:end-3) + dt_ds*(-(Ey_r(3:end-3,3:end-3) - Ey_r(2:end-4,3:end-3)) +  (Ex_r(3:end-3,3:end-3) - Ex_r(3:end-3,2:end-4)));
    Ex_r(2:end-3,2:end-3) = Ex_r(2:end-3,2:end-3) + dt_ds*C^2*(Bz_r(2:end-3,3:end-2) - Bz_r(2:end-3,2:end-3));
    Ey_r(2:end-3,2:end-3) = Ey_r(2:end-3,2:end-3) - dt_ds*C^2*(Bz_r(3:end-2,2:end-3) - Bz_r(2:end-3,2:end-3));


  
    %Exitation of source
    % Start and end position has to be tweeked for optimal results
    start = 150;
    endd = floor(2.0/ds);

    % Exiting beam on main grid
    Ex(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Exx.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
    Ey(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Eyy.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
    Ez(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Ezz.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
 
    % Exiting beam on reference grid. The endd position is slightly larger
    % than on teh main grid, at no reflections occur on teh reference grid
    endd = floor(2.5/ds);
    Ex_r(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Exx.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
    Ey_r(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Eyy.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);
    Ez_r(start:1:endd,3) = cos((k0*(x(start:1:endd)-x0)*(-cos(theta))-wt*n+pi/2)).*Ezz.*exp(-((x(start:1:endd)/sin(theta)-x0)/width).^2);

    
    %Insuring correct angle of initial beam
    t = [find(x(x(start:1:endd)>0+abs(C/(-cos(theta))*n*dt)))+(nx)-length(find(x(x>C/(-cos(theta))*n*dt)))];

     Ex(t+start, 3) = 0;
     Ey(t+start, 3) = 0;
     Ez(t+start, 3) = 0;

     Ex_r(t+start, 3) = 0;
     Ey_r(t+start, 3) = 0;
     Ez_r(t+start, 3) = 0;
    
%----- BOUNDARY CONDITIONS-----

    %MUR absorbing boundary conditions on main grid
    
    %Lower boundary  
    Ex_lower_now_1 = Ex(3:end-3, 3);  
    if n>1
        Ex(3:end-3,2) = -Ex_lower_pp_1(3:end-3) + K1*(Ex_lower_now_1+Ex_lower_pp_0(3:end-3)) ...
            + K2*(Ex_lower_p_0(3:end-3)+Ex_lower_p_1(3:end-3)) ...
            + K3*(Ex_lower_p_0(4:end-2)+Ex_lower_p_0(2:end-4) ...
            + Ex_lower_p_1(4:end-2)+Ex_lower_p_1(2:end-4));
    end
    
    Ex_lower_pp_1 = Ex_lower_p_1;
    Ex_lower_pp_0 = Ex_lower_p_0;
    Ex_lower_p_1 = Ex(:,3);
    Ex_lower_p_0 = Ex(:,2);
    
    Ey_lower_now_1 = Ey(3:end-3, 3);  
    if n>1
        Ey(3:end-3,2) = -Ey_lower_pp_1(3:end-3) + K1*(Ey_lower_now_1+Ey_lower_pp_0(3:end-3)) ...
            + K2*(Ey_lower_p_0(3:end-3)+Ey_lower_p_1(3:end-3)) ...
            + K3*(Ey_lower_p_0(4:end-2)+Ey_lower_p_0(2:end-4) ...
            + Ey_lower_p_1(4:end-2)+Ey_lower_p_1(2:end-4));
    end
    
    Ey_lower_pp_1 = Ey_lower_p_1;
    Ey_lower_pp_0 = Ey_lower_p_0;
    Ey_lower_p_1 = Ey(:,3);
    Ey_lower_p_0 = Ey(:,2);
    
    Ez_lower_now_1 = Ez(3:end-3, 3);  
    if n>1
        Ez(3:end-3,2) = -Ez_lower_pp_1(3:end-3) + K1*(Ez_lower_now_1+Ez_lower_pp_0(3:end-3)) ...
            + K2*(Ez_lower_p_0(3:end-3)+Ez_lower_p_1(3:end-3)) ...
            + K3*(Ez_lower_p_0(4:end-2)+Ez_lower_p_0(2:end-4) ...
            + Ez_lower_p_1(4:end-2)+Ez_lower_p_1(2:end-4));
    end
    
    Ez_lower_pp_1 = Ez_lower_p_1;
    Ez_lower_pp_0 = Ez_lower_p_0;
    Ez_lower_p_1 = Ez(:,3);
    Ez_lower_p_0 = Ez(:,2);

    
    % Upper boundary  
    Ex_upper_now_1 = Ex(3:end-3, end-4);
    if n>1
        Ex(3:end-3, end-3) = -Ex_upper_pp_1(3:end-3) + K1*(Ex_upper_now_1+Ex_upper_pp_0(3:end-3)) ...
            + K2*(Ex_upper_p_0(3:end-3)+Ex_upper_p_1(3:end-3)) ...
            + K3*(Ex_upper_p_0(4:end-2)+Ex_upper_p_0(2:end-4) ...
            + Ex_upper_p_1(4:end-2)+Ex_upper_p_1(2:end-4));
    end
    Ex_upper_pp_1 = Ex_upper_p_1;
    Ex_upper_pp_0 = Ex_upper_p_0;
    Ex_upper_p_1 = Ex(:,end-4);
    Ex_upper_p_0 = Ex(:,end-3);
    
    
    
    Ey_upper_now_1 = Ey(3:end-3, end-3);
    if n>1
        Ey(3:end-3, end-2) = -Ey_upper_pp_1(3:end-3) + K1*(Ey_upper_now_1+Ey_upper_pp_0(3:end-3)) ...
            + K2*(Ey_upper_p_0(3:end-3)+Ey_upper_p_1(3:end-3)) ...
            + K3*(Ey_upper_p_0(4:end-2)+Ey_upper_p_0(2:end-4) ...
            + Ey_upper_p_1(4:end-2)+Ey_upper_p_1(2:end-4));
    end
    Ey_upper_pp_1 = Ey_upper_p_1;
    Ey_upper_pp_0 = Ey_upper_p_0;
    Ey_upper_p_1 = Ey(:,end-3);
    Ey_upper_p_0 = Ey(:,end-2);
   
    
    
    Ez_upper_now_1 = Ez(3:end-3, end-3);
    if n>1
        Ez(3:end-3, end-2) = -Ez_upper_pp_1(3:end-3) + K1*(Ez_upper_now_1+Ez_upper_pp_0(3:end-3)) ...
            + K2*(Ez_upper_p_0(3:end-3)+Ez_upper_p_1(3:end-3)) ...
            + K3*(Ez_upper_p_0(4:end-2)+Ez_upper_p_0(2:end-4) ...
            + Ez_upper_p_1(4:end-2)+Ez_upper_p_1(2:end-4));
    end
    Ez_upper_pp_1 = Ez_upper_p_1;
    Ez_upper_pp_0 = Ez_upper_p_0;
    Ez_upper_p_1 = Ez(:,end-3);
    Ez_upper_p_0 = Ez(:,end-2);

    
    % Right boundary
    
    Ex_right_now_1 = Ex(end-3, 3:end-3);
    if n>1
        Ex(end-2,3:end-3) = -Ex_right_pp_1(3:end-3) + K1*(Ex_right_now_1+Ex_right_pp_0(3:end-3)) ...
            +K2*(Ex_right_p_0(3:end-3)+Ex_right_p_1(3:end-3)) ...
            + K3*(Ex_right_p_0(4:end-2)+Ex_right_p_0(2:end-4) ...
            + Ex_right_p_1(4:end-2)+Ex_right_p_1(2:end-4));
    end
    Ex_right_pp_1 = Ex_right_p_1;
    Ex_right_pp_0 = Ex_right_p_0;
    Ex_right_p_1 = Ex(end-3,:);
    Ex_right_p_0 = Ex(end-2,:);
    
    
    
    Ey_right_now_1 = Ey(end-4, 3:end-3);
    if n>1
        Ey(end-3,3:end-3) = -Ey_right_pp_1(3:end-3) + K1*(Ey_right_now_1+Ey_right_pp_0(3:end-3)) ...
            +K2*(Ey_right_p_0(3:end-3)+Ey_right_p_1(3:end-3)) ...
            + K3*(Ey_right_p_0(4:end-2)+Ey_right_p_0(2:end-4) ...
            + Ey_right_p_1(4:end-2)+Ey_right_p_1(2:end-4));
    end
    Ey_right_pp_1 = Ey_right_p_1;
    Ey_right_pp_0 = Ey_right_p_0;
    Ey_right_p_1 = Ey(end-4,:);
    Ey_right_p_0 = Ey(end-3,:);
    
    
    Ez_right_now_1 = Ez(end-3, 3:end-3);
    if n>1
        Ez(end-2,3:end-3) = -Ez_right_pp_1(3:end-3) + K1*(Ez_right_now_1+Ez_right_pp_0(3:end-3)) ...
            +K2*(Ez_right_p_0(3:end-3)+Ez_right_p_1(3:end-3)) ...
            + K3*(Ez_right_p_0(4:end-2)+Ez_right_p_0(2:end-4) ...
            + Ez_right_p_1(4:end-2)+Ez_right_p_1(2:end-4));
    end
    Ez_right_pp_1 = Ez_right_p_1;
    Ez_right_pp_0 = Ez_right_p_0;
    Ez_right_p_1 = Ez(end-3,:);
    Ez_right_p_0 = Ez(end-2,:);

    
    % Left boundary
    
    Ex_left_now_1 = Ex(3, 3:end-3);
    if n>1
        Ex(2,3:end-3) = -Ex_left_pp_1(3:end-3) + K1*(Ex_left_now_1+Ex_left_pp_0(3:end-3)) ...
            + K2*(Ex_left_p_0(3:end-3)+Ex_left_p_1(3:end-3)) ...
            + K3*(Ex_left_p_0(4:end-2)+Ex_left_p_0(2:end-4) ...
            + Ex_left_p_1(4:end-2)+Ex_left_p_1(2:end-4));
    end
    Ex_left_pp_1 = Ex_left_p_1;
    Ex_left_pp_0 = Ex_left_p_0;
    Ex_left_p_1 = Ex(3,:);
    Ex_left_p_0 = Ex(2,:);
    
 
    
    Ey_left_now_1 = Ey(3, 3:end-3);
    if n>1
        Ey(2,3:end-3) = -Ey_left_pp_1(3:end-3) + K1*(Ey_left_now_1+Ey_left_pp_0(3:end-3)) ...
            + K2*(Ey_left_p_0(3:end-3)+Ey_left_p_1(3:end-3)) ...
            + K3*(Ey_left_p_0(4:end-2)+Ey_left_p_0(2:end-4) ...
            + Ey_left_p_1(4:end-2)+Ey_left_p_1(2:end-4));
    end
    Ey_left_pp_1 = Ey_left_p_1;
    Ey_left_pp_0 = Ey_left_p_0;
    Ey_left_p_1 = Ey(3,:);
    Ey_left_p_0 = Ey(2,:);
    

    
    Ez_left_now_1 = Ez(3, 3:end-3);
    if n>1
        Ez(2,3:end-3) = -Ez_left_pp_1(3:end-3) + K1*(Ez_left_now_1+Ez_left_pp_0(3:end-3)) ...
            + K2*(Ez_left_p_0(3:end-3)+Ez_left_p_1(3:end-3)) ...
            + K3*(Ez_left_p_0(4:end-2)+Ez_left_p_0(2:end-4) ...
            + Ez_left_p_1(4:end-2)+Ez_left_p_1(2:end-4));
    end
    Ez_left_pp_1 = Ez_left_p_1;
    Ez_left_pp_0 = Ez_left_p_0;
    Ez_left_p_1 = Ez(3,:);
    Ez_left_p_0 = Ez(2,:);

 
    % Corners
    Ex(2,2)=Ex_left_pp_0(3);
    Ex(2,end-3)=Ex_left_pp_0(end-4);
    Ex(end-2,2)=Ex_right_pp_0(3);
    Ex(end-2,end-3)=Ex_right_pp_0(end-4);
    
    Ey(2,2)=Ey_left_pp_0(3);
    Ey(2,end-2)=Ey_left_pp_0(end-3);
    Ey(end-3,2)=Ey_right_pp_0(4);
    Ey(end-3,end-2)=Ey_right_pp_0(end-4);
    
    Ez(2,2)=Ez_left_pp_0(3);
    Ez(2,end-2)=Ez_left_pp_0(end-3);
    Ez(end-2,2)=Ez_right_pp_0(3);
    Ez(end-2,end-2)=Ez_right_pp_0(end-3);
    Ex(2,2) = 0;

    %Reference grid MUR absorbing BCs on upper boundary

    Ex_r_upper_now_1 = Ex_r(3:end-3, end-4);
    if n>1
        Ex_r(3:end-3, end-3) = -Ex_r_upper_pp_1(3:end-3) + K1*(Ex_r_upper_now_1+Ex_r_upper_pp_0(3:end-3)) ...
            + K2*(Ex_r_upper_p_0(3:end-3)+Ex_r_upper_p_1(3:end-3)) ...
            + K3*(Ex_r_upper_p_0(4:end-2)+Ex_r_upper_p_0(2:end-4) ...
            + Ex_r_upper_p_1(4:end-2)+Ex_r_upper_p_1(2:end-4));
    end
    Ex_r_upper_pp_1 = Ex_r_upper_p_1;
    Ex_r_upper_pp_0 = Ex_r_upper_p_0;
    Ex_r_upper_p_1 = Ex_r(:,end-4);
    Ex_r_upper_p_0 = Ex_r(:,end-3);
    
    
    
    Ey_r_upper_now_1 = Ey_r(3:end-3, end-3);
    if n>1
        Ey_r(3:end-3, end-2) = -Ey_r_upper_pp_1(3:end-3) + K1*(Ey_r_upper_now_1+Ey_r_upper_pp_0(3:end-3)) ...
            + K2*(Ey_r_upper_p_0(3:end-3)+Ey_r_upper_p_1(3:end-3)) ...
            + K3*(Ey_r_upper_p_0(4:end-2)+Ey_r_upper_p_0(2:end-4) ...
            + Ey_r_upper_p_1(4:end-2)+Ey_r_upper_p_1(2:end-4));
    end
    Ey_r_upper_pp_1 = Ey_r_upper_p_1;
    Ey_r_upper_pp_0 = Ey_r_upper_p_0;
    Ey_r_upper_p_1 = Ey_r(:,end-3);
    Ey_r_upper_p_0 = Ey_r(:,end-2);
   
    
    
    Ez_r_upper_now_1 = Ez_r(3:end-3, end-3);
    if n>1
        Ez_r(3:end-3, end-2) = -Ez_r_upper_pp_1(3:end-3) + K1*(Ez_r_upper_now_1+Ez_r_upper_pp_0(3:end-3)) ...
            + K2*(Ez_r_upper_p_0(3:end-3)+Ez_r_upper_p_1(3:end-3)) ...
            + K3*(Ez_r_upper_p_0(4:end-2)+Ez_r_upper_p_0(2:end-4) ...
            + Ez_r_upper_p_1(4:end-2)+Ez_r_upper_p_1(2:end-4));
    end
    Ez_r_upper_pp_1 = Ez_r_upper_p_1;
    Ez_r_upper_pp_0 = Ez_r_upper_p_0;
    Ez_r_upper_p_1 = Ez_r(:,end-3);
    Ez_r_upper_p_0 = Ez_r(:,end-2);

    
    %Reference grid perfectly reflecting boundary conditions on remaining boundaries
    
    %Left boundary
    Ey_r(2,:) = 0;
    Ez_r(2,:) = 0;
    
    %Right boundary
    Ey_r(end-2,:) = 0;
    Ez_r(end-2,:) = 0;
      
    %Lower boundary
    Ex_r(:,2) = 0;
    Ez_r(:,2) = 0;
    
    
    % Perfectly reflecting boundary conditions on fraction of upper boundary on the main grid
    Ex(1:nx/5, end-3) = 0;
    Ez(1:nx/5, end-2) = 0;
    
   
    % ---Calculationg O-X conversion transmission coefficient in each iteration---
    
    % Row to calculate it on (bottom row is 3)
    row = 4;
    
    % Position on lower boundary to start calculating
    start2 = 100;
    
    % Total power, incoming power and outgoing power
    power_tot = ds^2/dt*(E0/2*(((abs(Ex(start2:end-10,row)).^2))+((abs(Ey(start2:end-10,row)).^2))+((abs(Ez(start2:end-10,row)).^2)))...
        + 1/2*U0*(((abs(Bx(start2:end-10,row)).^2))+((abs(By(start2:end-10,row)).^2))+((abs(Bz(start2:end-10,row)).^2))))...
        + ds^2*(abs(Jx(start2:end-10,row).*Ex(start2:end-10,row)) + abs(Jy(start2:end-10,row).*Ey(start2:end-10,row)) + abs(Jz(start2:end-10,row).*Ez(start2:end-10,row)));
    power_in = ds^2/dt*(E0/2*(((abs(Ex_r(start2:end-10,row)).^2))+((abs(Ey_r(start2:end-10,row)).^2))+((abs(Ez_r(start2:end-10,row)).^2)))...
        + 1/2*U0*(((abs(Bx_r(start2:end-10,row)).^2))+((abs(By_r(start2:end-10,row)).^2))+((abs(Bz_r(start2:end-10,row)).^2))));
    power_out = power_tot-power_in;
    
    % Take the mean of the last 80 powercalculations (correspond to one wave period)
    vv = 0;
    if n>nt-80
        power_in_tot = power_in_tot + power_in;
        power_out_tot = power_out_tot + power_out;
        vv = vv + 1;
    end
    
    % Calculate efficiency and update list
    Efficiency = 1-sum(power_out(floor(0.57/ds)-100:floor(3.5/ds)-start2))/sum(power_in);
    Efficiency_list = [Efficiency_list, Efficiency];
end

%----- Plotting -------

% Find positions of cutoffs and resonances
[val,idx]=min(abs(X(ceil(nx/2), :)-1));
[val2,idx2]=min(abs(w_UH(ceil(nx/2), :)-w));
[val3,idx3]=min(abs(w_R(ceil(nx/2), :)-w));

% Plot the absolute value of the x-component of E on the reference grid
figure(1)
clf;
hold on
imagesc(ds*(1:1:nx),(ds*(1:1:ny_r))',abs(Ex_r)',[-1.0,1.0]);colorbar;
axis equal
colormap jet
title(['\fontsize{20} Time = ',num2str(round(n*dt*1e+12)),' ps', ', n=',num2str(n), ', Ex']); 
xlabel('x (in m)','FontSize',20);
ylabel('y (in m)','FontSize',20);
set(gca,'FontSize',20);


% Plot the evolution of the efficiency
figure(2)
clf;
hold on;
plot(1:1:n, Efficiency_list)


%% Plot of power profile
figure(3)
clf;
hold on;
plot(ds*(start2+89:1:nx-239), power_in_tot(90:end-229)./80, '--','LineWidth', 2, 'color', 'k')
plot(ds*(start2+89:1:nx-239), power_out_tot(90:end-229)./80,'-','LineWidth', 2, 'color', 'k')
legend({'Pin','Pout', 'Ptot'}, 'Location', 'NorthEast')
xlim([1,3.7])
xlabel('x [m]','FontSize',20);
ylabel('Power','FontSize',20);
set(gca,'FontSize',16);


%% Plot of absolute value of Ex on main grid

figure(4)
clf;
hold on
imagesc(ds*(1:1:nx),(ds*(1:1:ny))',abs(Ex)',[0, 0.8]);colorbar;
plot(ds*(1:1:nx), ones(nx,1)*idx*ds,'LineWidth',1.5,'color', 'w', 'LineStyle', '--')
plot(ds*(1:1:nx), ones(nx,1)*idx2*ds,'LineWidth',1.5,'color', 'w', 'LineStyle', '--')
plot(ds*(1:1:nx), ones(nx,1)*idx3*ds, 'LineWidth',1.5,'color', 'w', 'LineStyle', '--')
axis equal
colormap jet
title(['\fontsize{20} Time = ',num2str(round(n*dt*1e+12)),' ps', ', n=',num2str(n), ', Ex']); 
xlabel('x [m]','FontSize',20);
ylabel('y [m]','FontSize',20);
ylim([0,0.8])
xlim([1.0, 3.7])
set(gca,'FontSize',16);

%% Plot of |E| on main grid

figure(5)
clf;
hold on
E = sqrt(abs(Ey).^2+abs(Ex).^2+abs(Ez).^2);
imagesc(ds*(1:1:nx),(ds*(1:1:ny))',E',[0, 1.0]);colorbar;
plot(ds*(1:1:nx), ones(nx,1)*idx*ds,'LineWidth',1.5,'color', 'k', 'LineStyle', '--')
plot(ds*(1:1:nx), ones(nx,1)*idx2*ds,'LineWidth',1.5,'color', 'k', 'LineStyle', '--')
plot(ds*(1:1:nx), ones(nx,1)*idx3*ds, 'LineWidth',1.5,'color', 'k', 'LineStyle', '--')
axis equal
colormap jet
title(['\fontsize{20} Time = ',num2str(round(n*dt*1e+12)),' ps', ', n=',num2str(n), ', E']); 
xlabel('x [m]','FontSize',20);
ylabel('y [m]','FontSize',20);
set(gca,'FontSize',16);
ylim([0,0.8])
xlim([0.57, 3.3])