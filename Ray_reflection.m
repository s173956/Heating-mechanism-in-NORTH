function [R_O, R_X] = Ray_reflection(equifile,DS,BS,frq,machine,N,ret,i,in_unit, out_unit,n, mode)

% Check that input variables are given
if ~exist('DS') DS = 1; end
if ~exist('BS') BS = 1; end
if ~exist('frq') frq = 105; end
if ~exist('machine') machine = 'ASDEX'; end
if ~exist('figno') figno = 60; end

if ~isstruct(equifile)
    a = load(equifile);
else
    a = equifile;
    disp('equi struct supplied by user')
end

%----Definition of grids ----

%------Define grids and variables

% Source frequency
w = frq*2*pi;

%Nature constants
natconst;

%Mass of ion species
massratio=2*1836;

%Grid with plasma electron density
ne_grid = interp1(a.psi_prof,a.ne_prof, a.psi_grid)*1e19;

% Spatial grid with size of magnetic field
Bmod_grid = BS * sqrt(a.BR_grid.^2 + a.Bz_grid.^2 + a.Bphi_grid.^2);

% Grids for plasma frequency and electron cyclotron frequency
wpe_grid =  sqrt(DS*ne_grid*Qe.^2/(E0*Me));
wpi_grid = wpe_grid/sqrt(massratio);
wp_grid = sqrt(wpe_grid.^2+wpi_grid.^2);
wce_grid = Qe*Bmod_grid/Me;

%Spatial coordinates
radial = transpose(sqrt(ret(N).r(1:(end-i),1).^2+ret(N).r(1:(end-i),2).^2));
z = transpose(ret(N).r(1:(end-i),3));

%Spatial meshgrid
[R,Z] = meshgrid(a.R_grid,a.z_grid);

%Lists with plasma frequency and electron cyclotron frequency
wp_list = interp2(R,Z, wp_grid , radial, z);
wce_list = interp2(R,Z, wce_grid, radial, z);

% Convert lists to X,Y coordinates
X = wp_list.^2/w.^2;
Y = wce_list/w;

% CUrrent position
r0 = sqrt(   ret(N).r((end-i),1).^2 +  ret(N).r((end-i),2).^2     );
z0 = ret(N).r((end-i),3);

% Interpolating magnetic field components at current position
BR = interp2(R,Z,a.BR_grid,r0,z0);
Bz = interp2(R,Z,a.Bz_grid,r0,z0);
Bphi = interp2(R,Z,a.Bphi_grid,r0,z0);

% Incident phi angle
phi = atan2(ret(N).r((end-i),2), ret(N).r((end-i),1));

% Expressing the B-field in Cartesian coordinates
Bx = cos(phi)*BR - sin(phi)*Bphi;
By = sin(phi)*BR + cos(phi)*Bphi;
Bvec = [Bx, By, Bz];

% Basis vectors for Stix basis
z_stix_i = Bvec/norm(Bvec);
x_stix_i = (in_unit- dot(in_unit,Bvec)/dot(Bvec,Bvec)*Bvec)/norm(in_unit- dot(in_unit,Bvec)/dot(Bvec,Bvec)*Bvec);
y_stix_i = cross(z_stix_i,x_stix_i);

% Incident theta angle
theta = acos(dot(Bvec, in_unit)/(norm(Bvec)*norm(in_unit)));

%Check if theta has the correct sign by looking at angle to y-axis
beta = acos(dot(in_unit, x_stix_i)/(norm(x_stix_i)*norm(in_unit)));

if beta < pi/2
    theta = -theta;
end

%Defininion of gamma factor and refractive index from the
%Alter-Appleton-Hartree relation
Gamma = ((Y(end)).^4*sin(theta).^4+4*(Y(end)).^2*(1-X(end)).^2*cos(theta).^2).^(1/2);
N2O = 1- (2*X(end)*(1-X(end)))/(2*(1-X(end))-(Y(end)).^2*sin(theta).^2+Gamma);
N2X = 1- (2*X(end)*(1-X(end)))/(2*(1-X(end))-(Y(end)).^2*sin(theta).^2-Gamma);

% Choose the dispersion relation to used based on ray polarization
if mode == "O"
    N2 = N2O;
elseif mode == "X"
    N2 = N2X;
end
    
% Define E-field in stix geometry using the polarization in eq (39)-(40)
% and normalising
Ex_stix_i = -5i+3;
Ey_stix_i = 1i*Y(end)/(1-(Y(end)).^2*((1-N2)/X(end))-1)*Ex_stix_i;
Ez_stix_i = -N2*cos(theta)*sin(theta)/(1-X(end)-N2*sin(theta).^2)*Ex_stix_i;

%normalising
E_stix_i = [Ex_stix_i, Ey_stix_i, Ez_stix_i]/norm([Ex_stix_i, Ey_stix_i, Ez_stix_i]);

% Basis vectors in new basis with z pointing along the
% direftion of k_i (k_i_basis)
x_k_i = rodrigues_rot(x_stix_i, y_stix_i, -theta);
y_k_i = y_stix_i;
z_k_i = rodrigues_rot(z_stix_i, y_stix_i, -theta);

% Change of basis matrix
M_k_i = transpose([x_k_i; y_k_i; z_k_i]);
M_stix_i = transpose([x_stix_i; y_stix_i; z_stix_i]);
Change_of_basis = inv(M_stix_i)*M_k_i;

% Changing the basis of E
E_k_i = Change_of_basis*transpose(E_stix_i);
E_k_i(3)=0;

% Incident angle in new basis (Vessel basis) with z pointing along the
% normal vector of the vessel wall
gamma = acos(dot(in_unit, -out_unit)/(norm(in_unit)*norm(out_unit)));

% Basis vectors in Vessel basis
z_normal = -n;
x_normal = rodrigues_rot(x_k_i, cross(in_unit, -n), gamma/2);
y_normal = y_k_i;

% Change of basis matrix from k_i basis to Vessel basis
Mn = transpose([x_normal; y_normal; z_normal]);
Change_of_basis = inv(Mn)*M_k_i;

% Change basis of E-field
E_n_i = Change_of_basis*E_k_i;

% Apply boundary conditions to define the polarization of the reflected ray
% in the Vessel basis
E_n_r = [-E_n_i(1), -E_n_i(2), E_n_i(3)];

% Basis vectors in new basis with z pointing along the
% direftion of the reflected k vactor k_r (k_r_basis)
z_k_r = out_unit;
x_k_r = rodrigues_rot(x_normal, cross(n, out_unit), gamma/2);
y_k_r = y_normal;
x_k_r = -x_k_r;

% Change of basis matrix from vessel basis to k_r_basis
M_k_r = transpose([x_k_r; y_k_r; z_k_r]);
Change_of_basis = inv(M_k_r)*Mn;

% Change basis of the polarization (E-field) of the reflected wave
E_k_r = Change_of_basis*transpose(E_n_r);

% Basis vecotrs in new Stix basis for the reflected ray
z_stix_r = Bvec/norm(Bvec);
x_stix_r = (out_unit- dot(out_unit,Bvec)/dot(Bvec,Bvec)*Bvec)/norm(out_unit- dot(out_unit,Bvec)/dot(Bvec,Bvec)*Bvec);
y_stix_r = cross(z_stix_r,x_stix_r);

% Change of basis matrix from k_r_basis to new Stix basis
M_stix_r = transpose([x_stix_r; y_stix_r; z_stix_r]);
Change_of_basis = inv(M_k_r)*M_stix_r;

% Angle between k_r and z-axis
theta2 = acos(dot(Bvec, out_unit)/(norm(Bvec)*norm(out_unit)));

%Defininion of gamma factor and refractive index from the
%Alter-Appleton-Hartree relation
Gamma = ((Y(end)).^4*sin(theta2).^4+4*(Y(end)).^2*(1-X(end)).^2*cos(theta2).^2).^(1/2);
N2O = 1- (2*X(end)*(1-X(end)))/(2*(1-X(end))-(Y(end)).^2*sin(theta2).^2+Gamma);
N2X = 1- (2*X(end)*(1-X(end)))/(2*(1-X(end))-(Y(end)).^2*sin(theta2).^2-Gamma);

% polarization of reflected O-mode
Ex_O = 1;
Ey_O = -2i/(Y(end)^2*sin(theta2).^2+sqrt(Y(end).^2*sin(theta2).^4+4*cos(theta2).^2))*Ex_O;
Ez_O = -1/cot(theta2)*Ex_O;

% Use the polarization as a basis vector
O_basis = [Ex_O, Ey_O, Ez_O]/norm([Ex_O, Ey_O, Ez_O]);

% Change coordinate system to k_r
O_k_r = Change_of_basis*transpose(O_basis);

% If a tiny fraction of polarization in the z-direction in the k_r_basis
% remains, it is remoed. (It should be zero, as the waves are transverse)
O_k_r(3)=0;

% polarization of reflected X-mode
N2 = N2X;
Ey_X = 1;
Ex_X = -1i/Y(end)*((1-N2)/X(end)*(1-Y(end).^2)-1)*Ey_X;
Ez_X = -N2*cos(theta2)*sin(theta2)/(1-X(end)-N2*sin(theta2).^2)*Ex_X;

% Use the polarization as a basis vector
X_basis = [Ex_X, Ey_X, Ez_X]/norm([Ex_X, Ey_X, Ez_X]);

% Change coordinate system to k_r
X_k_r = Change_of_basis*transpose(X_basis);

% If a tiny fraction of polarization in the z-direction in the k_r_basis
% remains, it is remoed. (It should be zero, as the waves are transverse)
X_k_r(3) = 0;

% Project the polarization (E-field) of the reflected ray into the two
% basis vectors (consisting of the O- and X-polarization vectors)
O = (E_k_r(1)*X_k_r(2)-E_k_r(2)*X_k_r(1))/...
    (O_k_r(1)*X_k_r(2)-O_k_r(2)*X_k_r(1));
X = -(E_k_r(1)*O_k_r(2)-E_k_r(2)*O_k_r(1))/...
    (O_k_r(1)*X_k_r(2)-O_k_r(2)*X_k_r(1));

% Compute fraction reflected power polarized as O- and X- mode respectively
R_O = norm(O)/(norm(O)+norm(X));
R_X = norm(X)/(norm(O)+norm(X));

end