clear;
close;

%---------------- INIT -----------------

%This is the main script than runs the ray tracins simulations. Both mode
%conversions and reflections are implemented. 

% Apart form the Warm ray code, the script requires the functions
% Ray_reflection, Transmission and CMA_diagram-


%empty lists

%Bool_list keeps track of which rays that needs to be traced and which have
%already been traced. At all launch positions exceot the first, both a O-
%and X-mode is launched, if the remaining power in each of the rays exceeds
%the threshold.

bool_list = [];

%list to keep track of the power content of all traced rays
power_content_list = [];

%List of launch positions and directions
R_K_LAUN0_list = [];

%Parameter to keep track of code crashes
o = 0;

%Initial power absorbtion due to ECR damping
A = 0;

%Inial absorbtion due to the formation of EBWs
EBW = 0;

%Initial total power absorbtion
A_tot = 0;

% -------Parameters-----
% Initial ray power
power_content = 1;
% Loss at reflections
loss = 0.1;
% power threshold to stop tracing 
Lower_power_limit = 0.01;
% Spacing in psi-histogram
psi_spacing = 0.04;


% user defined values
aa.STEP = 2*1e-3;
aa.equidir=[cd '/']; % equil location
aa.equilib='NORTH_02'; % name of equilibrium
aa.RLAUN0 = 'LFS'; % launch position 
aa.KPOL = "true";
aa.KLAUN0 = [90, 60 ,0.0]; % %change second value to change poliodal injection angle
aa.FRQ = 2.45e9; % frequency
aa.MODE = 'O';
aa.BS = 1; % magnetic scaling
aa.DS = 3; % density field scaling
aa.TS = 1; % Temperature scaling
aa.wdir = cd;


%-------------- First ray -------------------

%Try to launch a ray or move launch position by a fraction
cdp = cd;
p = 0;
    while 1 > 0
        try
            wdir = NORTHwr2(aa);
            ret = loadwr3(wdir);
            p = 1;
        catch
            o = o + 1;
            aa.RLAUN0 = aa.RLAUN0 + [0, 0.001, 0];
        end
        if p == 1
            break
        end
    end

    
aa.KPOL = "false"; %For the rest of the code, only use cartesian coordinates are used


% Stop ray at boundaries
Nj = length(ret);
u = 0;
for i=0:(length(ret(Nj).r(:,1))-5)
    if sqrt((sqrt(ret(Nj).r(end-i,1).^2+ret(Nj).r(end-i,2).^2)-0.25).^2+(ret(Nj).r(end-i,3).^2)) < 0.125
        if u == 0      
            u = i;
        end
    end
    if sqrt((sqrt(ret(Nj).r(end-i,1).^2+ret(Nj).r(end-i,2).^2)-0.25).^2+(ret(Nj).r(end-i,3).^2)) > 0.125
        u = 0;
    end
end
i = u;
e = i;

%Check is the ray reaches the plasma cutoff (or is within X=0.98)
if aa.MODE == 'O'
    conversion = 0;
    [X, Y] = get_X_Y([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,Nj,ret,i);
    for n=1:length(X)
        if X(n)>1
            conversion = 1;
            m = n;
            i = length(ret(Nj).r(:,1))-m;
            break
        end
    end
    
    if conversion == 0
        for n=1:length(X)
            if X(n)>0.98
                conversion = 1;
                m = n;
                i = length(ret(Nj).r(:,1))-m;
                break
            end
        end
    end
end


if i == 0
    i = 1;
end


% Calculate absorption of the ray
[psi_list, dA_tot] = dpsi_dA([aa.equidir aa.equilib], 'NORTH', Nj,ret,i,power_content, psi_spacing);
tau = ret(Nj).tau(end-i);
if tau<0
    tau = 0;
end
A = 1-exp(-tau);
power_content = 1*exp(-tau);

%Add the absorbtion to a list
A_list = [A];
dA_list = [sum(dA_tot)];

%If an O-mode reaches the plasma cutoff, a mode conversion is executed.
%This is followed by a reflection
if conversion == 1
    
    %Transmission coefficient
    T = Ray_transmission([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,Nj,ret,length(X)-m);
    if T>1
        T=1;
    end
    %Update launch position and direction
    RLAUN0 = [ret(Nj).r(m,1), ret(Nj).r(m,2), ret(Nj).r(m,3)];
    KLAUN0 = [ret(Nj).r(m,1)-ret(Nj).r(m-1,1), ret(Nj).r(m,2)-ret(Nj).r(m-1,2), ...
    ret(Nj).r(m,3)-ret(Nj).r(m-1,3)];

    %Update lists with power content, the list boollist to keep track
    %of rays that are traced, and a list to store launch positions and directions for
    %future launches
    power_content_list = cat(1, power_content_list, power_content*[0, T]);
    bool_list = cat(1, bool_list, [1, 0]);
    R_K_LAUN0_list = cat(1, R_K_LAUN0_list, [RLAUN0, KLAUN0]);
    
    %Power in reflected ray
    reflected_power = power_content*(1-T);
    
    %Reflect k-vector
    theta = -atan2( ret(Nj).r(end-e,2), ret(Nj).r(end-e,1))+pi/2;    
    phi = atan2( ret(Nj).r(end-e,3), sqrt(ret(Nj).r(end-e,1).^2+ret(Nj).r(end-e,2).^2)-0.25);
    n = -[sin(theta)*cos(phi), cos(theta)*cos(phi), sin(phi)];
    in_dir = [ret(Nj).r(end-e,1)-ret(Nj).r(end-(e+1),1), ret(Nj).r(end-e,2)-ret(Nj).r(end-(e+1),2), ...
        ret(Nj).r(end-e,3)-ret(Nj).r(end-(e+1),3)];  
    in_unit = in_dir/norm(in_dir);
    out_unit = 2*dot(n,-in_unit)*n-(-in_unit);

    %Calculate fraction of reflectoed O- and X-mode using the function
    %Ray_reflection
    [R_O, R_X] = Ray_reflection([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,e, in_unit, out_unit,n, aa.MODE);
    
    %Define new launch position and direction
    RLAUN0 = [(ret(Nj).r(end-e,1)+ret(Nj).r((end-e+1),1))/2, (ret(Nj).r(end-e,2)+ret(Nj).r((end-e+1),2))/2, (ret(Nj).r(end-e,3)+ret(Nj).r((end-e+1),3))/2];
    KLAUN0 = out_unit;
    
    %Update power content list, bool_list and R_K_LAUN0_list
    power_content_list = cat(1, power_content_list, power_content*(1-T)*(1-loss)*[R_O, R_X]);
    bool_list = cat(1, bool_list, [0, 0]);
    R_K_LAUN0_list = cat(1, R_K_LAUN0_list, [RLAUN0, KLAUN0]);
else
    %If the ray doesn't undergo a mode cinversion, it is reflected
    
    %Reflect k-vector
    theta = -atan2( ret(Nj).r(end-i,2), ret(Nj).r(end-i,1))+pi/2;    
    phi = atan2( ret(Nj).r(end-i,3), sqrt(ret(Nj).r(end-i,1).^2+ret(Nj).r(end-i,2).^2)-0.25);
    n = -[sin(theta)*cos(phi), cos(theta)*cos(phi), sin(phi)];
    in_dir = [ret(Nj).r(end-i,1)-ret(Nj).r(end-(i+1),1), ret(Nj).r(end-i,2)-ret(Nj).r(end-(i+1),2), ...
        ret(Nj).r(end-i,3)-ret(Nj).r(end-(i+1),3)];  
    in_unit = in_dir/norm(in_dir);
    out_unit = 2*dot(n,-in_unit)*n-(-in_unit);

    %Calculate fraction of reflectoed O- and X-mode using the function
    %Ray_reflection
    [R_O, R_X] = Ray_reflection([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,i, in_unit, out_unit,n, aa.MODE);

    %Define new launch position and direction
    RLAUN0 = [(ret(Nj).r(end-i,1)+ret(Nj).r((end-i+1),1))/2, (ret(Nj).r(end-i,2)+ret(Nj).r((end-i+1),2))/2, (ret(Nj).r(end-i,3)+ret(Nj).r((end-i+1),3))/2];
    KLAUN0 = out_unit;
    
    %Update power content list, bool_list and R_K_LAUN0_list
    power_content_list = cat(1, power_content_list, power_content*(1-loss)*[R_O, R_X]);
    bool_list = cat(1, bool_list, [0, 0]);
    R_K_LAUN0_list = cat(1, R_K_LAUN0_list, [RLAUN0, KLAUN0]);
end



%------ PLOT first ray ------

% Line styles
if aa.MODE == 'O'
   style = '-';
else
    style = ':';
end

%Color map
map = copper(100);
map = flip(map, 1);
power_content = 1;

%plot cross section  
figure(1);
clf;
equil_plot_UH_general([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ*1e-9,'NORTH',1)
hold on
R0=0.25;a0=0.125;ang=0:0.01:2*pi;
plot((a0)*cos(ang)+R0,(a0)*sin(ang),'k','linewidth',2);
if conversion == 0
    plot(sqrt(ret(Nj).r(1:(end-i),1).^2+ret(Nj).r(1:(end-i),2).^2),ret(Nj).r(1:(end-i),3), style, 'LineWidth', 2, 'color', map(floor(100*power_content),:))
else
    plot(sqrt(ret(Nj).r(1:m,1).^2+ret(Nj).r(1:m,2).^2),ret(Nj).r(1:m,3), style, 'LineWidth', 2, 'color', map(floor(100*power_content),:))
    if reflected_power<0
        reflected_power = 0.001;
    elseif reflected_power == 0
        reflected_power = 0.001;
    end
    plot(sqrt(ret(Nj).r(m:(end-e),1).^2+ret(Nj).r(m:(end-e),2).^2),ret(Nj).r(m:(end-e),3), style, 'LineWidth', 2, 'color', map(ceil(100*reflected_power),:))

end
axis([0.1 0.4 -0.15 0.15 ])

% plot of projection in toroidal plane
figure(2); 
clf;
hold on;
set(gca,'fontsize',16,'linewidth',1.5);
if conversion == 0
    plot(ret(Nj).r(1:(end-i),1),ret(Nj).r(1:(end-i),2),style, 'linewidth',2,'color', map(floor(100*power_content),:));
else
    plot(ret(Nj).r(1:m,1),ret(Nj).r(1:m,2),style, 'linewidth',2,'color', map(floor(100*power_content),:));
    plot(ret(Nj).r(m:(end-e),1),ret(Nj).r(m:(end-e),2),style, 'linewidth',2,'color', map(ceil(100*reflected_power),:));
end
colormap(flip(copper,1))
colorbar;
plot(ret(Nj).r(1:(end-i),1),ret(Nj).r(1:(end-i),2),style, 'linewidth',2,'color', map(floor(100*power_content),:));
axis([-0.4 0.4 -0.4 0.4 ])
R0 = 0.25;a0=0.125;ang=0:0.01:2*pi;
plot((R0-a0)*cos(ang),(R0-a0)*sin(ang),'k','linewidth',2);
plot((R0+a0)*cos(ang),(R0+a0)*sin(ang),'k','linewidth',2);
plot((0.3447)*cos(ang),(0.3447)*sin(ang),'k','linestyle','-');
xlabel('x [m]');
ylabel('y [m]');

%CMA plot
figure(5);
clf;
hold on;
if conversion == 0
    Ray_CMA_plot([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,i, aa.MODE,5, map, power_content)
else
    Ray_CMA_plot([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,e, aa.MODE,5, map, power_content)
end


% Start loop through the bool_list until all rays are traced
while ~all(all((bool_list)))
    for n=0:(length(bool_list(:,1))-1)
        %First trace O-mode
        if bool_list(end-n, 1) == 0
            bool_list(end-n, 1) = 1;
            power_content = power_content_list(end-n, 1);
            aa.MODE = 'O';
            aa.RLAUN0 = R_K_LAUN0_list(end-n, 1:3);
            aa.KLAUN0 = R_K_LAUN0_list(end-n, 4:6);
            break
        %Otherwise trace X-mode
        elseif bool_list(end-n, 2) == 0
            bool_list(end-n, 2) = 1;
            power_content = power_content_list(end-n, 2);
            aa.MODE = 'X';
            aa.RLAUN0 = R_K_LAUN0_list(end-n, 1:3);
            aa.KLAUN0 = R_K_LAUN0_list(end-n, 4:6);
            break
        end
    end
    
    %Check that the power content in te ray is above the threshold
    if power_content < Lower_power_limit
        continue
    end

    %Launch ray
    cdp = cd;
    wdir = NORTHwr2(aa);
    p = 0;
    while 1 > 0
        try
            wdir = NORTHwr2(aa);
            ret = loadwr3(wdir);
            p = 1;
        catch
            o = o + 1;
            aa.RLAUN0 = aa.RLAUN0 - [0.0001, 0, 0];
        end
        if p == 1
            break
        end
    end
    
    
   %Check for mode conversions
    Nj = length(ret);
    u = 0;
    for i=0:(length(ret(Nj).r(:,1))-5)
        if sqrt((sqrt(ret(Nj).r(end-i,1).^2+ret(Nj).r(end-i,2).^2)-0.25).^2+(ret(Nj).r(end-i,3).^2)) < 0.125
            if u == 0      
                u = i;
            end
        end
        if sqrt((sqrt(ret(Nj).r(end-i,1).^2+ret(Nj).r(end-i,2).^2)-0.25).^2+(ret(Nj).r(end-i,3).^2)) > 0.125
            u = 0;
        end
    end
    i = u;
    e = i;
    
    conversion = 0;
    if aa.MODE == 'O'
        [X, Y] = get_X_Y([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,Nj,ret,i);
        for n=1:length(X)
            if X(n)>1
                conversion = 1;
                m = n;
                i = length(ret(Nj).r(:,1))-m;
                break
            end
        end
        if conversion == 0
            for n=1:length(X)
                if X(n)>0.98
                    conversion = 1;
                    m = n;
                    i = length(ret(Nj).r(:,1))-m;
                    break
                end
            end
        end
    end

    %Check if an  X-mode reaches the UH layer
    UH = 0;
    if aa.MODE == 'X'
        if length(ret(Nj).r(:,1)) > 0
            if sqrt((sqrt(ret(Nj).r(end,1).^2+ret(Nj).r(end,2).^2)-0.25).^2+(ret(Nj).r(end,3).^2)) < 0.125
                UH = 1;
            end
        end
    end

    %Update tay, A and lists
    tau = ret(Nj).tau(end-i);
    if tau<0
        tau = 0;
    end
    A = A + power_content*(1-exp(-tau));
    [psi_list, dA] = dpsi_dA([aa.equidir aa.equilib], 'NORTH', Nj,ret,i,power_content, psi_spacing);
    dA_tot = dA_tot + dA;
    
    A_list = [A_list, A];
    dA_list = [dA_list, sum(dA_tot)];
    
    %Calculate power content in ray
    current_power = power_content;
    power_content = power_content*exp(-tau);
    
    
    % Keep track of power in generated EBWs 
    if UH == 1
        EBW = EBW + power_content;
    end
    
    % If the ray isn't an X-mode reaching teh upper hybrid layer, check for
    % mode ocnversions
    if UH == 0
        if conversion == 1
            %Transmission coefficient
            T = Ray_transmission([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,Nj,ret,length(X)-m);
            
            %Update launch position and direction
            RLAUN0 = [ret(Nj).r(m,1), ret(Nj).r(m,2), ret(Nj).r(m,3)];    
            KLAUN0 = [ret(Nj).r(m,1)-ret(Nj).r(m-1,1), ret(Nj).r(m,2)-ret(Nj).r(m-1,2), ...
            ret(Nj).r(m,3)-ret(Nj).r(m-1,3)];

            %Update lists with power content, the list boollist to keep track
            %of rays that are traced, and a list to store launch positions and directions for
            %future launches
            power_content_list = cat(1, power_content_list, power_content*[0, T]);
            bool_list = cat(1, bool_list, [1, 0]);
            R_K_LAUN0_list = cat(1, R_K_LAUN0_list, [RLAUN0, KLAUN0]);
            reflected_power = power_content*(1-T);

            % Reflection of k-vector
            theta = -atan2( ret(Nj).r(end-e,2), ret(Nj).r(end-e,1))+pi/2;    
            phi = atan2( ret(Nj).r(end-e,3), sqrt(ret(Nj).r(end-e,1).^2+ret(Nj).r(end-e,2).^2)-0.25);
            n = -[sin(theta)*cos(phi), cos(theta)*cos(phi), sin(phi)];
            in_dir = [ret(Nj).r(end-e,1)-ret(Nj).r(end-(e+1),1), ret(Nj).r(end-e,2)-ret(Nj).r(end-(e+1),2), ...
                ret(Nj).r(end-e,3)-ret(Nj).r(end-(e+1),3)];  
            in_unit = in_dir/norm(in_dir);
            out_unit = 2*dot(n,-in_unit)*n-(-in_unit);

            %Calculate fraction of reflectoed O- and X-mode using the function
            %Ray_reflection
            [R_O, R_X] = Ray_reflection([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,e, in_unit, out_unit,n, aa.MODE);

            %Define new launch position and direction
            RLAUN0 = [(ret(Nj).r(end-e,1)+ret(Nj).r((end-e+1),1))/2, (ret(Nj).r(end-e,2)+ret(Nj).r((end-e+1),2))/2, (ret(Nj).r(end-e,3)+ret(Nj).r((end-e+1),3))/2];
            KLAUN0 = out_unit;
            
            %Update power content list, bool_list and R_K_LAUN0_list
            power_content_list = cat(1, power_content_list, power_content*(1-loss)*(1-T)*[R_O, R_X]);
            bool_list = cat(1, bool_list, [0, 0]);
            R_K_LAUN0_list = cat(1, R_K_LAUN0_list, [RLAUN0, KLAUN0]);
            
        else
            %If the ray doesn't undergo a mode cinversion, it is reflected
            
            % Reflection of k-vector
            theta = -atan2( ret(Nj).r(end-i,2), ret(Nj).r(end-i,1))+pi/2;    
            phi = atan2( ret(Nj).r(end-i,3), sqrt(ret(Nj).r(end-i,1).^2+ret(Nj).r(end-i,2).^2)-0.25);
            n = -[sin(theta)*cos(phi), cos(theta)*cos(phi), sin(phi)];
            in_dir = [ret(Nj).r(end-i,1)-ret(Nj).r(end-(i+1),1), ret(Nj).r(end-i,2)-ret(Nj).r(end-(i+1),2), ...
                ret(Nj).r(end-i,3)-ret(Nj).r(end-(i+1),3)];  
            in_unit = in_dir/norm(in_dir);
            out_unit = 2*dot(n,-in_unit)*n-(-in_unit);

            %Calculate fraction of reflectoed O- and X-mode using the function
            %Ray_reflection
            [R_O, R_X] = Ray_reflection([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,i, in_unit, out_unit,n, aa.MODE);

            %Define new launch position and direction
            RLAUN0 = [(ret(Nj).r(end-i,1)+ret(Nj).r((end-i+1),1))/2, (ret(Nj).r(end-i,2)+ret(Nj).r((end-i+1),2))/2, (ret(Nj).r(end-i,3)+ret(Nj).r((end-i+1),3))/2];
            KLAUN0 = out_unit;

            %Update power content list, bool_list and R_K_LAUN0_list
            power_content_list = cat(1, power_content_list, power_content*(1-loss)*[R_O, R_X]);
            bool_list = cat(1, bool_list, [0, 0]);
            R_K_LAUN0_list = cat(1, R_K_LAUN0_list, [RLAUN0, KLAUN0]);
        end
    end

    % Line style depending on mode
    if aa.MODE == 'O'
       style = '-';
    else
        style = ':';
    end


    %plot cross section  
    figure(1);
    hold on;
    b = gca; legend(b,'off');
    if conversion == 0
        if current_power>1
            current_power = 1;
        end
        if current_power < 0
            current_power = 0.000001;
        elseif isnan(current_power)
            current_power = 0.00001;
        end
        plot(sqrt(ret(Nj).r(1:(end-i),1).^2+ret(Nj).r(1:(end-i),2).^2),ret(Nj).r(1:(end-i),3), style, 'LineWidth', 2, 'color', map(ceil(100*current_power),:))
    else
        if reflected_power < 0
            reflected_power = 0.000001;
        end
        plot(sqrt(ret(Nj).r(1:m,1).^2+ret(Nj).r(1:m,2).^2),ret(Nj).r(1:m,3), style, 'LineWidth', 2, 'color', map(ceil(100*current_power),:))
        plot(sqrt(ret(Nj).r(m:(end-e),1).^2+ret(Nj).r(m:(end-e),2).^2),ret(Nj).r(m:(end-e),3), style, 'LineWidth', 2, 'color', map(ceil(100*reflected_power),:))
     end
    
    % plot top view
    figure(2); 
    hold on;
    if conversion == 0
        plot(ret(Nj).r(1:(end-i),1),ret(Nj).r(1:(end-i),2),style, 'linewidth',2,'color', map(ceil(100*current_power),:));
    else
        if reflected_power < 0
            reflected_power = 0.000001;
        end

        plot(ret(Nj).r(1:m,1),ret(Nj).r(1:m,2),style, 'linewidth',2,'color', map(ceil(100*current_power),:));
        plot(ret(Nj).r(m:(end-e),1),ret(Nj).r(m:(end-e),2),style, 'linewidth',2,'color', map(ceil(100*reflected_power),:));
    end
    
    %CMA plot
    figure(5);
    hold on;
    if conversion == 0
        Ray_CMA_plot([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,i, aa.MODE,5, map, current_power)
    else
         Ray_CMA_plot([aa.equidir aa.equilib],aa.DS,aa.BS,aa.FRQ,'NORTH',Nj,ret,e, aa.MODE,5, map, current_power)
    end
end

%calculate total absorbed power 
A_tot = A + EBW;

%% plot absorbtion as a function of psi
figure(6)
hb = bar(psi_list, dA_tot*1/sum(dA_tot));
set(hb,'FaceColor',[0.2 0.2 0.2 ])
ylabel('Fraction of absorption');
xlabel('\psi');
set(gca,'FontSize',16);